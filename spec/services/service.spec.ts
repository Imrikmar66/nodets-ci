import itemService, { Item } from '../../app/item.service';

describe('Item Service', () => {

    it('Send an item', () => {

        let item: Item|null = itemService.find(1);
        expect( item ).toBeTruthy();
        item = <Item> item;
        expect( item.id ).toBeTruthy();
        expect( item.title ).toBeTruthy();
        item = <Item>item;
        expect( item.id ).toBeGreaterThan(0);
        expect(item.title.length).toBeGreaterThan(0);

    });

    it('Send null', () => {

        let item: Item|null = itemService.find(0);
        expect( item ).toBeNull();

    });

});