import * as request from 'request';
import itemService, { Item } from '../../app/item.service';
import server from '../../app/server';
import { Server } from 'http';
const base_url = 'http://localhost:3000';

let activeServer: Server;

describe('Express Server', () => {

    beforeAll((done) => {
        activeServer = server.listen( 3000, () => { 
            console.log(' - Launching test server');
            done();
        } );
    });

    afterAll(() => {
        console.log(' - Closing test server');
        activeServer.close();
    });

    describe('GET /', () => {

        it('returns status code 200', ( done ) => {

            request.get(base_url, (error, response, body) => {

                expect( response.statusCode ).toBe( 200 );
                done();

            });

        });

        it('returns Hello world', (done) => {

            request.get(base_url, (error, response, body) => {

                expect( body ).toBe( 'Hello world' );
                done();

            });

        });

    });

    describe('GET /:id', () => {
        
        describe('Getting an existing item', () => {

            const id = 1;
    
            it('returns status code 200', ( done ) => {
    
    
                request.get(`${base_url}/${id}`, (error, response, body) => {
    
                    expect( response.statusCode ).toBe( 200 );
                    done();
    
                });
    
            });
    
            it('return correct item', (done) => {
    
                request.get(`${base_url}/${id}`, (error, response, body) => {
    
                    const item = JSON.parse( body );
                    
                    expect( item ).toBeTruthy();
                    expect( item.id ).toBe( id );
                    expect( item.title ).not.toBe( null );
                    expect( item.title ).toBe( (<Item>itemService.find( id )).title );
                    done();
    
                });
    
            });

        });

        describe('Getting an unexisting item', () => {

            const id = 5;
    
            it('returns status code 404', ( done ) => {
    
    
                request.get(`${base_url}/${id}`, (error, response, body) => {
    
                    expect( response.statusCode ).toBe( 404 );
                    done();
    
                });
    
            });
    
            it('return not found', (done) => {
    
                request.get(`${base_url}/${id}`, (error, response, body) => {
    
                    expect( body ).toBe( 'Not found' );
                    done();
    
                });
    
            });

        });
        

    });

});