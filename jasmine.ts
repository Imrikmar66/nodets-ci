const 
    Jasmine = require('jasmine'),
    ijasmine = new Jasmine(),
    JasmineConsoleReporter = require('jasmine-console-reporter');

    ijasmine.loadConfigFile('spec/support/jasmine.json');

const reporter = new JasmineConsoleReporter({
    colors: 1,
    cleanStack: 1,
    verbosity: 4,
    listStyle: 'indent',
    timeUnit: 'ms',
    timeThreshold: { ok: 500, warn: 1000, ouch: 3000 },
    activity: false,
    emoji: true,
    beep: true
});

ijasmine.env.clearReporters();
ijasmine.addReporter( reporter );
ijasmine.execute();