import * as express from 'express';
import itemService from './item.service';

const app: express.Application = express();

app.use(express.static('public')),

app.get( '/', ( req: express.Request, res: express.Response ) => {

    res.send('Hello world');

});

app.get( '/:id', ( req: express.Request, res: express.Response ) => {

    const item = itemService.find( req.params.id );
    if( item ){
        res.setHeader('Content-Type', 'application/json');
        res.send( JSON.stringify(item) );
    }
    else {
        res.status(404).send('Not found');
    }

});

export default app;