export interface Item {
    id: number;
    title: string;
}
class ItemService {

    private db: Item[];

    constructor() {
        this.db = [
            {
                id: 1,
                title: 'item 1'
            },
            {
                id: 2,
                title: 'item 2'
            },
            {
                id: 3,
                title: 'item 3'
            }
        ];
    }

    find( id: number ): Item | null {

        const filtered = this.db.filter( ( item ) => item.id == id );
        if( filtered.length ) return filtered[0];
        else return null;
        
    }

}

export default new ItemService;