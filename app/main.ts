import server from './server';

const port: number = 9000;
const activeServer = server.listen( port, () => { console.log( `Listening on port ${port}` ); });

let ticker = 0;
const interval = setInterval(() => {
    ticker ++;
    console.log( `Tick ${ticker}` );
    if(ticker >= 10) {
        activeServer.close();
        clearInterval( interval );
        console.log('...end app');
    }
}, 1000);