const 
    path = require('path'),
    nodeExternals = require('webpack-node-externals');

module.exports = {
    mode: 'development',
    watch: true,
    entry: './app/main.ts',
    target: 'node',
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'bundle.js'
    },
    externals: [nodeExternals()],
    devtool: 'inline-source-map',
    resolve: {
        extensions: ['.ts']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader'
            }
        ]
    }
}