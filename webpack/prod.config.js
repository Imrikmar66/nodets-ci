const 
    path = require('path'),
    nodeExternals = require('webpack-node-externals');

module.exports = {
    mode: 'production',
    entry: './app/main.ts',
    target: 'node',
    devtool: false,
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'bundle.js'
    },
    externals: [nodeExternals()],
    resolve: {
        extensions: ['.ts']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader'
            }
        ]
    }
}