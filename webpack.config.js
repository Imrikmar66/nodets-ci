let CONFIG;

module.exports = (function(env){

    if( env && env.production == 'true' )
        CONFIG = require('./webpack/prod.config');

    else
        CONFIG = require('./webpack/dev.config');

    return CONFIG;

});